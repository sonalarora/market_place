# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import datetime
YEAR_CHOICES = [(r,r) for r in range(1984, datetime.date.today().year+1)]


class ResPartner(models.Model):
    _inherit = "res.partner"

    whats_app_number = fields.Char(
        string='WhatsApp No', required="1"
    )
    linkdin = fields.Char("Linkedin")
    twitter = fields.Char("Twitter")
    facebook = fields.Char("Facebook")
    instagram = fields.Char("Instagram")
    google_plus = fields.Char("Google Plus")
    company_name = fields.Char("Company Name")
    singup = fields.Selection([
            ('freelancer', 'Freelancer'),
            ('system_integrator', 'System Integrator'),
            ('customer', 'Customer'),
        ], 'Signup')
    year = fields.Many2one('year.master', string='Year Started')
    subscription_plan = fields.Many2one('subscription.plan', string='Subscription Plan')
    total_project_implemented = fields.Char('Total Projects Implemented')
    state = fields.Selection([
                              ('draft', 'Draft'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected'), ],
                             default='draft', string="State")
    group_template = fields.Many2one('group.rights','Group Template')

    def button_approval(self):
        group_internal = self.env.ref('base.group_user')
        group_marketplace = self.env.ref('MKS_MARKETPLACE.group_stock_marketplace')
        for groups in self.group_template.groups:
            self.user_ids.write({'active': True, 'groups_id': [
                                    (4, group_internal.id),
                                    (4, groups.id)]})
        self.write({
            'state': 'approved'
        })
        return

    def button_reject(self):
        self.write({
            'state': 'rejected'
        })
        return
    @api.model
    def create(self, vals):
        if 'country_id' in vals and vals.get('country_id', ''):
            if isinstance(vals.get('country_id', ''), str):
                vals.update({'country_id': int(vals.get('country_id', ''))})
        if 'subscription_id' in vals and vals.get('subscription_id', ''):
            if isinstance(vals.get('subscription_id', ''), str):
                vals.update({'subscription_plan': int(vals.get('subscription_id', ''))})
        print('vals++++++++++++++', vals)
        return super(ResPartner, self).create(vals)

class Users(models.Model):
    _inherit = "res.users"


    def set_home_actions(self):
        print('set_home_actio%%%%%%%%%%%%nset_home_actionset_home_action')
        has_users = self.env['res.users'].sudo().search([])
        for user in has_users:
            # group_portal = self.env.ref('base.group_portal')
            # print('group_portal++++++++++++++', group_portal)
            # if group_portal:
            #     return self.env['ir.actions.actions'].sudo().search([('name','=','Website Shop')])
            # else:
            #     print('else==============')
            return self.env['ir.actions.actions'].sudo().search([('name','=','Website Shop')])

    action_id = fields.Many2one('ir.actions.actions',  string='Home Action',default='set_home_actions',
        help="If specified, this action will be opened at log on for this user, in addition to the standard menu.")

    @api.constrains('groups_id')
    def _check_one_user_type(self):
        """We check that no users are both portal and users (same with public).
           This could typically happen because of implied groups.
        """
        user_types_category = self.env.ref('base.module_category_user_type', raise_if_not_found=False)
        user_types_groups = self.env['res.groups'].search(
            [('category_id', '=', user_types_category.id)]) if user_types_category else False
        # if user_types_groups:  # needed at install
        #     print('user_types_groups++++++++', user_types_groups)
        #     if self._has_multiple_groups(user_types_groups.ids):
        #         raise ValidationError(_('The user cannot have more than one user types.'))

class AwardsCertification(models.Model):
    _name = "awards.certified.resources"

    certification_name = fields.Char('Certificate Name', required="1")
    year = fields.Many2one('year.master', string='Year',required="1")
    awarded_by = fields.Char(string="Awarded By")
    certificate_uploaded = fields.Many2many('ir.attachment', string="Upload Certificate",
                                         help='You can attach your document', copy=False)
    certificate_id = fields.Many2one('product.template', string='Certificate Id')

class ProjectImplemented(models.Model):
    _name = "project.implemented"

    industry = fields.Char('Industry', required="1")
    project_name = fields.Char("Project Name", required="1")
    project_details = fields.Text('Project Details', required="1")
    project_id = fields.Many2one('product.template', string="Project Id")

class WOrkExperience(models.Model):
    _name = "work.experience"

    company_id = fields.Char(string='Company', required="1")
    title = fields.Char("Title", required="1")
    detail_of_work = fields.Text('Details Of The Work')
    from_date = fields.Date('From Date')
    to_date = fields.Date('To Date')
    work_id = fields.Many2one('product.template', string="Work Id")

class Educations(models.Model):
    _name = "resource.education"

    certification_name = fields.Char('Certificate Name', required="1")
    year = fields.Many2one('year.master', string='Year', required="1")
    awarded_by = fields.Char(string="Awarded By", required="1")
    education_id = fields.Many2one('product.template', string='Education Id')

class CertifiedResources(models.Model):
    _name = "certified.resource"

    # s_no = fields.Char("S.No")
    technology = fields.Char('Technology', required="1")
    no_of_resources = fields.Char('No Of Resources', required="1")
    certified_id = fields.Many2one('product.template', string='Certified Id')

class ScopeOfTheProject(models.Model):
    _name = "scope.project"

    # s_no = fields.Char("S.No")
    activity = fields.Many2one('activity.master', string='Activity', required="1")
    included = fields.Char('Included (Y/N)', required="1")
    scope_id = fields.Many2one('product.template', string='Project Scope Id')

class PaymentPlan(models.Model):
    _name = "payment.plan"

    # s_no = fields.Char("S.No")
    milestone = fields.Char('Milestone', required="1")
    of_paymet = fields.Char('% Of Payment', required="1")
    remark = fields.Text('Remarks')
    plan_id = fields.Many2one('product.template', string='Payment Plan Id')

class Rrfrences(models.Model):
    _name = "resource.refrences"

    # s_no = fields.Char("S.No")
    title = fields.Char('Title', required="1")
    name = fields.Char('Name', required="1")
    designation = fields.Char('Designation', required="1")
    contact_no = fields.Char(string='Contact No', required="1")
    email = fields.Char(required="1")
    refrences_id = fields.Many2one('product.template', string='Refrence Id')

class terms_conditions(models.Model):
    _name = "terms.conditions"

    # s_no = fields.Char("S.No")
    terms_conditions = fields.Text('Details', required="1")
    term_id = fields.Many2one('product.template', string='Term Id')

# Activity Master
class ActivityMaster(models.Model):
    _name = "activity.master"

    name = fields.Char('Activity', required="1")

# Year Master
class YearMaster(models.Model):
    _name = "year.master"

    name = fields.Char('Year', required="1")

class ProductTemplate(models.Model):
    _inherit = "product.template"

    certificate_resorce_ids = fields.One2many('awards.certified.resources', 'certificate_id', string='Certificate Reso Ids',copy=True)
    project_ids = fields.One2many('project.implemented', 'project_id', string='Project Ids', copy=True)
    work_ids = fields.One2many('work.experience', 'work_id', string='Work Ids', copy=True)
    education_ids = fields.One2many('resource.education', 'education_id', string='Education Ids', copy=True)
    certified_ids = fields.One2many('certified.resource', 'certified_id', string='Cerified Ids', copy=True)
    scope_ids = fields.One2many('scope.project', 'scope_id', string='Scope Ids', copy=True)
    plan_ids = fields.One2many('payment.plan', 'plan_id', string='Plan Ids', copy=True)
    refrences_ids = fields.One2many('resource.refrences', 'refrences_id', string='Refrences', copy=True)
    term_ids = fields.One2many('terms.conditions', 'term_id', string='Terms', copy=True)
    milestone = fields.Char('Milestone')
    vendor_id = fields.Many2one('res.partner', string='Vendor')
    street = fields.Char(required="1")
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char(required="1")
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', required="1")
    phone = fields.Char()
    mobile = fields.Char(required="1")
    whats_app_number = fields.Char(
        string='WhatsApp No',
    )
    linkdin = fields.Char("Linkedin")
    twitter = fields.Char("Twitter")
    facebook = fields.Char("Facebook")
    instagram = fields.Char("Instagram")
    google_plus = fields.Char("Google Plus")
    email = fields.Char()
    note = fields.Text('About Us', required="1")
    service_overview = fields.Text('Service Overview', required="1")
    banner_image = fields.Image("Image", required="1")
    year = fields.Many2one('year.master', string='Year')
    total_project_implemented = fields.Char('Total Project Implemented')

    @api.onchange('vendor_id')
    def change_vendor_id(self):
        for rec in self:
            rec.street = rec.vendor_id.street
            rec.street2 = rec.vendor_id.street2
            rec.zip = rec.vendor_id. zip
            rec.city = rec.vendor_id.city
            rec.state_id = rec.vendor_id.state_id
            rec.country_id = rec.vendor_id.country_id
            rec.phone = rec.vendor_id.phone
            rec.mobile = rec.vendor_id.mobile
            rec.whats_app_number = rec.vendor_id.whats_app_number
            rec.linkdin = rec.vendor_id.linkdin
            rec.twitter = rec.vendor_id.twitter
            rec.facebook = rec.vendor_id.facebook
            rec.instagram  = rec.vendor_id.instagram
            rec.google_plus = rec.vendor_id.google_plus
            rec.email = rec.vendor_id.email,
            rec.year = rec.vendor_id.year.id,
            rec.total_project_implemented = rec.vendor_id.total_project_implemented

class GroupRights(models.Model):
    _name = "group.rights"

    name = fields.Char('Name', required="1")
    groups = fields.Many2many('res.groups', string='Groups')
    group_type = fields.Selection([('vendor', 'Vendor'),('customer','Customer')
        ])


