# -*- coding: utf-8 -*-

import logging
import werkzeug

from odoo import _, api, fields, models, SUPERUSER_ID
from odoo import http, tools
from odoo.addons.auth_signup.models.res_users import SignupError
from odoo.addons.web.controllers.main import ensure_db, Home
from odoo.addons.base_setup.controllers.main import BaseSetup
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.addons.website_sale.controllers.main import TableCompute,WebsiteSale
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.exceptions import UserError
from odoo.http import content_disposition, Controller, request, route
from odoo.osv import expression
from odoo.addons.website.controllers.main import QueryURL

_logger = logging.getLogger(__name__)


class CustomAuthSignupHome(AuthSignupHome):    
    def do_signup(self, qcontext):
        """ Override do_signup for Create User & Partner with Extra field Mobile.
        """
        print('qcontext%%%%%%%%%%%%%%%%%%%',qcontext)
        # print('vendor+++++++++++++++++', vendor)
        values = {key: qcontext.get(key) for key in ('login', 'name', 'password','whats_app_number','mobile','comment','singup','company_name','country_id','supplier_rank','subscription_plan')}
        if values.get('singup') != 'customer':
            vendor = request.env['group.rights'].sudo().search([('group_type','=','vendor')])
            values.update({'supplier_rank':1,'group_template':vendor.id})
        else:
            vendor = request.env['group.rights'].sudo().search([('group_type','!=','vendor')])
            values.update({'group_template':vendor.id})
        assert values.get('password') == qcontext.get('confirm_password'), "Passwords do not match; please retype them."
        supported_langs = [lang['code'] for lang in request.env['res.lang'].sudo().search_read([], ['code'])]
        if request.lang in supported_langs:
            values['lang'] = request.lang
        self._signup_with_values(qcontext.get('token'), values)
        request.env.cr.commit()

