# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'MKS Market Place',
    'version': '1.0',
    'category': 'Market Place',
    'summary': '',
    'description': "",
    'depends': ['subscription_management','wk_appointment','odoo_e_wallet','calendar','subscription_management','website_product_label','contacts','hr','web','auth_signup','portal','stock','project','product','base','calendar','purchase'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'security/calender_security.xml',
        'views/res_partner_view.xml',
        'views/product_template_views.xml',
        'views/webclient_template_login.xml',
        'views/customer_signup_template.xml',
        'views/market_place_vendor.xml',
        'views/product_label.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
