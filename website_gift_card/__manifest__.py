# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Website Gift Cards",
  "summary"              :  """Website Gift Cards allows you to create and sell gift cards in Odoo Website.""",
  "category"             :  "Website",
  "version"              :  "3.1.1",
  "sequence"             :  10,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "website"              :  "https://store.webkul.com/Odoo-Website-Gift-Cards.html",
  "description"          :  """Website Gift Cards
Odoo Website Gift Cards
Website Gift Cards in Odoo
Gift Cards in Odoo Website
Gift Cards
Gift Vouchers
Gift Coupons
E-Gift Vouchers
Sell Gift Cards
Offer Gift Card Voucher
Odoo Module for Gift Cards
Sell gift coupons
Custom Gift Cards""",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=website_gift_card&version=12.0&custom_url=/shop",
  "depends"              :  [
                             'mail',
                             'website_voucher',
                            ],
  "data"                 :  [
                             'views/inherited_product_view.xml',
                             'views/inherited_sale_view.xml',
                             'views/inherited_voucher_view.xml',
                             'views/gift_card_view.xml',
                             'views/res_config_view.xml',
                             'views/templates.xml',
                             'data/email_template.xml',
                             'views/report_view.xml',
                             'security/ir.model.access.csv',
                            ],
  "demo"                 :  ['data/demo.xml'],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  74,
  "currency"             :  "USD",
  "pre_init_hook"        :  "pre_init_check",
}