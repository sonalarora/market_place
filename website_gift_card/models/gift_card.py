#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
from datetime import datetime, timedelta
import logging
_logger = logging.getLogger(__name__)


class GiftCardMessages(models.Model):
	_name = "gift.card.messages"
	_description = "Gift Card Messages"
	
	msg_email_card = fields.Char(
		string='e-Gift Card Message In Website',
		translate=True,
		default="This is an e-card and the secret code will be sent via email.",
		help="Message to be displayed on website for e-gift card",
		)
	msg_physical_card = fields.Char(
		string='Physical Gift Card Message In Website',
		translate=True,
		default="This is a physical gift card and the secret code will delivered physically.",
		help="Message to be displayed on website for physical gift card",
		)
	
	def save(self):
		self.ensure_one()
		view_id = self.env.ref('website_gift_card.website_gift_card_inherited_res_config_settings_view_form').id
		return {
			'type': 'ir.actions.act_window',
			'res_model': 'res.config.settings',
			'view_type': 'form',
			'view_id': view_id,
			'view_mode': 'form',
			'target': 'inline',
			'context':{'module' : 'wk_coupons'}
		}

	