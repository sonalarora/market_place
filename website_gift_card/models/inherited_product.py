#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    wk_is_gift_card = fields.Boolean(
        string='Is Gift Card')
    wk_gift_card_type = fields.Selection(
        [('email','E-mail Gift Card'),
        ('physical','Physical Gift Card')],
        string="Gift Card Type",
        default="email",
        help="In E-mail Gift Card an email will be send to the customer and in the Physical Gift Card a hard copy need to be delivered"
        )
    wk_card_email_template = fields.Many2one(
        comodel_name="mail.template",
        string='Email Template to be Sent',
        help="On purchasing of the gift card this email template will be sent to the customer."
    )
    wk_validity = fields.Integer(
        string="Validity",
        default="1",
    )
    wk_validity_unit = fields.Selection(
        [('months','Months'),('years','Years')],
        default="years",
        string="Validity Unit")
    wk_is_partially_redemed = fields.Boolean(
        string='Use Partial Redemption',
        default=True,
        help="Enable this option partial redemption option.")
    wk_redeemption_limit = fields.Integer(
        string='Max Redemption Limit',
        default=-1,
        help="The maximum number of times the coupon can be redeemed. -1 means the coupon can be used any number of times untill the voucher value is Zero.")

    @api.model
    def create(self, vals):
        if vals.get('wk_is_gift_card'):
            if  vals.get('wk_gift_card_type') == 'email':
                vals['type'] = 'service'
            if vals.get('wk_gift_card_type') == 'physical':
                vals['type'] = 'product'
        return super(ProductTemplate, self).create(vals)
    
    
    def write(self, vals):
        if vals.get('wk_gift_card_type') == 'email':
            vals['type'] = 'service'
        if vals.get('wk_gift_card_type') == 'physical':
            vals['type'] = 'product'
        return super(ProductTemplate, self).write(vals)
    
    @api.model
    def get_gift_card_message(self):
        msg = ""
        config_id = self.env['ir.default'].sudo().get('res.config.settings','gift_card_message_id')
        if config_id:
            msg_id = self.env['gift.card.messages'].sudo().browse(config_id)
            if self.wk_gift_card_type == 'email':
                msg = msg_id.msg_email_card
            if self.wk_gift_card_type == 'physical': 
                msg = msg_id.msg_physical_card
        return msg

    @api.model
    def get_validity_msg(self):
        msg = ''
        if self.wk_validity_unit == 'months':
            msg = "Validity of this gift card is %s months."%self.wk_validity
        else:
            msg = "Validity of this gift card is %s years."%self.wk_validity
        return msg
