#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
import logging
_logger = logging.getLogger(__name__)

class VoucherVoucher(models.Model):
    _inherit = "voucher.voucher"

    gift_card_voucher = fields.Boolean(
        string="Gfit Card Voucher")
    order_line_id = fields.Many2one(
        comodel_name="sale.order.line",
        string="Order Line Id",
        help="This will be used for the domain purpose.")

    @api.model
    def get_report_string(self, string=''):
        history_obj = self.env['voucher.history'].search([('voucher_id','=',self.id),('transaction_type','=','credit')])
        if history_obj.sale_order_line_id and history_obj.sale_order_line_id.product_id.wk_is_gift_card and history_obj.sale_order_line_id.product_id.wk_gift_card_type =='physical':
            if string == 'reciever_name':
                return history_obj.sale_order_line_id.wk_gift_card_to
            if string == 'sender_name':
                return history_obj.sale_order_line_id.wk_gift_card_from
            if string == 'message':
                return history_obj.sale_order_line_id.wk_gift_card_extra_message
        return False


    @api.model
    def get_gift_card_voucher_values(self, line_id):
        product_id = line_id.product_id
        
        #------------------------------------coded by ankit--------------------------------------#
        IrDefault = self.env['ir.default'].sudo()
        use_partial_redemption = IrDefault.get('res.config.settings', 'wk_coupon_partially_use' ) or False
        if use_partial_redemption:
            total_available = IrDefault.get('res.config.settings', 'wk_coupon_partial_limit' ) or 1
        else:
            total_available = 1
        #----------------------------------------------------------------------------------------#
    
        if product_id.wk_validity_unit == 'months':
            exp_date = str(datetime.today().date()+ relativedelta(months=product_id.wk_validity))
        else:
            exp_date = str(datetime.today().date()+ relativedelta(years=product_id.wk_validity))
        vals = {
            'voucher_value':product_id.lst_price,
            'expiry_date':exp_date,
            'voucher_val_type':'amount',
            'use_minumum_cart_value':False,
            'is_partially_redemed':product_id.wk_is_partially_redemed,
            'voucher_usage':'ecommerce',
            'total_available':total_available,
            'issue_date':str(datetime.today().date()),
            'available_each_user':1,
            'customer_type':'general',
            'applied_on':'all',
            'name':product_id.name,
            'validity':0,
            'gift_card_voucher':True,
            'order_line_id':line_id.id,
        }
        return vals

    @api.model
    def create_gift_card_voucher(self, line_id):
        vals  = self.get_gift_card_voucher_values(line_id)
        qty = line_id.product_uom_qty
        while qty > 0:
            try:
                vals.pop('message_follower_ids', False)
                voucher_id = self.create(vals)
                vals.pop('voucher_code', False)
                vocuher_history_obj = self.env['voucher.history'].sudo().search([('voucher_id','=',voucher_id.id),('transaction_type','=','credit')], limit=1)
                if vocuher_history_obj:
                    vocuher_history_obj.order_id = line_id.order_id.id
                    vocuher_history_obj.sale_order_line_id = line_id.id
                    vocuher_history_obj.description = "Voucher Created at %s"%str(datetime.today())
            except Exception as e:
                _logger.info('-------Exception in Creating Gift Card Voucher------%r',e)
                pass
            qty -= 1
        return True

class VoucherHistory(models.Model):
    _inherit = "voucher.history"

    def send_e_gift_card_email(self):
        msg = 'Email with the secret code has been sent successfully to the customer.'
        if not self.sale_order_line_id:
            raise ValidationError('You can not send email of this voucher because there is no sale order line.')
        if self.sale_order_line_id.product_id.wk_is_gift_card:
            if self.sale_order_line_id.product_id.wk_gift_card_type == 'physical':
                raise ValidationError('You can not send email of this gift card because this is a physical gift card and you need to send it physically.')
            else:
                res = self.send_email(self.sale_order_line_id.product_id)
                if not res:
                    msg = "There is an error while sending the email. Please try again."
        wizard_id = self.env['wizard.message'].create({'text':msg}).id
        return { 'name':_("Summary"),
                'view_mode': 'form',
                'view_id': False,
                'view_type': 'form',
                'res_model': 'wizard.message',
                'res_id': wizard_id,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                }

    @api.model
    def send_email(self, product_id):
        temp_id = product_id.wk_card_email_template
        res = ''
        if temp_id:
            try:
                temp_id.send_mail(self.id, force_send=True,raise_exception=True)
                self.state = 'done'
                self.description += "\n Email sent at %s "%str(datetime.today())
            except Exception as e:
                self.state = 'fail'
                self.description += "\nEmail failed at %s "%str(datetime.today())
                return False
        return True

    @api.model
    def get_gift_card_voucher_code(self):
        code = self.voucher_id.voucher_code
        return code
