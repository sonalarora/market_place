#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)

class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	gift_card_message_id = fields.Many2one(
		comodel_name = "gift.card.messages",
		string='Gift Card Messages')
	
	def configure_messages_for_gift_card(self):
		self.ensure_one()
		view_id = self.env.ref( 'website_gift_card.gift_card_messages_form').id
	  
		res_id =  self.env['ir.default'].sudo().get('res.config.settings','gift_card_message_id')
		if not res_id:
			xid = self.env['ir.model.data'].search([('module', '=', 'website_gift_card'), ('name', '=', 'gift_card_messages')])
			if xid:
				res_id = xid.res_id
				res_id = self.env.ref( 'website_gift_card.gift_card_messages').id
				self.env['ir.default'].sudo().set('res.config.settings', 'gift_card_message_id',res_id)

		return {
			'name': 'Gift Card Configuration',
			'view_type': 'form',
			'view_mode': 'form',
			'views': [(view_id, 'form')],
			'res_model': 'gift.card.messages',
			'view_id': view_id,
			'type': 'ir.actions.act_window',
			'target': "inline",
			'res_id': res_id,
		}
	
	def set_values(self):
		super(ResConfigSettings, self).set_values()
		IrDefault = self.env['ir.default'].sudo()
		IrDefault.set('res.config.settings', 'gift_card_message_id',self.gift_card_message_id.id)
		return True

	@api.model
	def get_values(self):
		res = super(ResConfigSettings, self).get_values()
		IrDefault = self.env['ir.default'].sudo()
		res.update(
			{
			'gift_card_message_id':IrDefault.get('res.config.settings', 'gift_card_message_id')
			}
		)
		return res