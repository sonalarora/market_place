#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import ValidationError

class SaleOrder(models.Model):
	_inherit = "sale.order"

	@api.depends('order_line')
	def _is_e_gift_card_present(self):
		for record in  self:
			record.wk_e_gift_card_exists = False
			for line in record.order_line:
				if line.wk_gift_card == "email":
					record.wk_e_gift_card_exists = True
					break

	wk_e_gift_card_exists = fields.Boolean(
		compute="_is_e_gift_card_present",
		string="Has e Gift Card"
	)

	def action_confirm(self):
		for order in self:
			for line in order.order_line:
				if line.product_id.wk_is_gift_card:
					if line.product_id.wk_gift_card_type == 'physical':
						while line.product_uom_qty > 1:
							line_vals = {'product_id':line.product_id.id,
										'product_uom_qty':1,
										'price_unit' : line.price_unit,
										'tax_id':[(6,0,line.tax_id.ids)],
										'customer_lead':line.customer_lead,
										'name':line.name,
										'order_id':order.id
							}
							new_line_id = self.env['sale.order.line'].create(line_vals)
							self.env['voucher.voucher'].create_gift_card_voucher(new_line_id)
							new_line_id.wk_gift_card = 'physical'
							line.product_uom_qty -= 1
						self.env['voucher.voucher'].create_gift_card_voucher(line)
						line.wk_gift_card = 'physical'
					else:
						line.wk_gift_card = 'email'
						self.env['voucher.voucher'].create_gift_card_voucher(line)
		return super(SaleOrder, self).action_confirm()

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	wk_gift_card = fields.Selection(
		[('email','Email'),
		('physical','Physical')],
		string="Has e-Gift card")
	wk_gift_card_to = fields.Char(
		string="To")
	wk_gift_card_from = fields.Char(
		string='From')
	wk_gift_card_extra_message = fields.Text(
		string="Message")
	wk_gift_card_reciever_email = fields.Char(
		string='Reciever Email')


	def send_e_gift_card_vouchers_via_emails(self):
		history_ids = self.env['voucher.history'].search([('sale_order_line_id','=', self.id)])
		msg = ''
		success = 0
		fail = 0
		done = 0
		if len(history_ids) != self.product_uom_qty:
			raise ValidationError('Quantity of the Order line is not equal to the vouchers asociated with this line. Please equalize the quantity to proceed.')
		for history_line in history_ids:
			if history_line.state == 'done':
				done += 1
			else:
				res = history_line.send_email(history_line.sale_order_line_id.product_id)
				if res:
					success += 1
				else:
					fail += 1
		msg = "%s number of emails have been sent successfully!!"%success
		if fail:
			msg += "<br/> %s number of emails have been failed."%fail
		if done:
			msg += "<br/> %s number of emails have been sent already."%done
		wizard_id = self.env['wizard.message'].create({'text':msg}).id
		return { 'name':_("Summary"),
				'view_mode': 'form',
				'view_id': False,
				'view_type': 'form',
				'res_model': 'wizard.message',
				'res_id': wizard_id,
				'type': 'ir.actions.act_window',
				'nodestroy': True,
				'target': 'new',
				'domain': '[]',
				}

	def print_physical_gift_card_voucher(self):
		if self.product_id.wk_is_gift_card:
			if self.product_id.wk_gift_card_type == 'physical':
				history_id = self.env['voucher.history'].search([('sale_order_line_id','=',self.id),('transaction_type','=','credit')], limit=1)
				if history_id:
					return self.env.ref('wk_coupons.coupons_report').report_action(history_id.voucher_id)
				else:
					raise ValidationError('There is not any voucher in this order line.')
			else:
				raise ValidationError('This voucher needs to be send via email, you can not print this.')
		else:
			raise ValidationError('There is not any voucher in this order line')
		return True
