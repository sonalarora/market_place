#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2018-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
from odoo import api, fields, models, _
from datetime import datetime
class Website(models.Model):
	_inherit = 'website'

	def wk_get_customer_vouchers(self):
		partner_id = self.env['res.users'].browse(self._uid).partner_id.id
		current_date = datetime.now().date()
		voucher_ids = self.env['voucher.voucher'].search([('expiry_date','>=',current_date),('issue_date','<=',current_date),('customer_type','=','general'),('voucher_usage','!=','pos'),('gift_card_voucher','=',False)])
		voucher_ids += self.env['voucher.voucher'].search([('expiry_date','>=',current_date),('issue_date','<=',current_date),('customer_id','=',partner_id),('customer_type','=','special_customer'),('voucher_usage','!=','pos'),('gift_card_voucher','=',False)])
		return voucher_ids