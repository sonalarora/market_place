
#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2019-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
import logging
from odoo import http
from odoo.http import request
from odoo.tools.translate import _
from odoo.addons.website_sale.controllers.main import WebsiteSale
_logger = logging.getLogger(__name__)

class WebsiteSale(WebsiteSale):
    
    @http.route(['/gift/card/address'], type='http', auth="public", website=True)
    def gift_card_address(self, **post):
        if post.get('line_id'):
            line_id = post.get('line_id')
            line_obj = request.env['sale.order.line'].sudo().browse(int(line_id))
            line_obj.wk_gift_card_to =str(post.get('wk_gift_card_to'))
            line_obj.wk_gift_card_from = str(post.get('wk_gift_card_from'))
            line_obj.wk_gift_card_reciever_email = str(post.get('wk_gift_card_reciever_email'))
            line_obj.wk_gift_card_extra_message = str(post.get('wk_gift_card_extra_message'))
        return request.redirect('/shop/cart')

    @http.route(['/get/gift/address/values'], type='json', auth="public", website=True)
    def get_gift_card_values(self, line_id=False, **post):
        vals = {}
        if line_id:
            line_obj = request.env['sale.order.line'].sudo().browse(line_id)
            vals = {'wk_gift_card_to':line_obj.wk_gift_card_to or None,
                    'wk_gift_card_from':line_obj.wk_gift_card_from or None,
                    'wk_gift_card_reciever_email':line_obj.wk_gift_card_reciever_email or None,
                    'wk_gift_card_extra_message':line_obj.wk_gift_card_extra_message or None,
                    'gift_type':str(line_obj.product_id.wk_gift_card_type),
            }
        return vals
