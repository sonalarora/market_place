    /* Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>) */
/* See LICENSE file for full copyright and licensing details. */
odoo.define('website_gift_card.website_gift_card', function (require) {
	var ajax = require('web.ajax');
    $(document).ready(function(){
        var oe_website_sale = this;
        $('.gift-card-address-modal').on("click",function(event){
                
                $("input[name*='wk_gift_card_reciever_email']").parent().show();
                $("input[name*='wk_gift_card_reciever_email']").parent().removeClass('has-error');
                var line_id = $(this).data('id');
                $(".modal-body #modal-line-id").val(line_id);
                ajax.jsonRpc("/get/gift/address/values", 'call', {'line_id':line_id})
                .then(function (data) {
                    if (data['gift_type'] =='physical') {
                        $("input[name*='wk_gift_card_reciever_email']").parent().hide();
                    }
                    $("input[name*='wk_gift_card_to']").val(data['wk_gift_card_to']);
                    $("input[name*='wk_gift_card_from']").val(data['wk_gift_card_from']);
                    $("input[name*='wk_gift_card_reciever_email']").val(data['wk_gift_card_reciever_email']);
                    $("textarea[name*='wk_gift_card_extra_message']").val(data['wk_gift_card_extra_message']);
                });
                $("input[name*='wk_gift_card_reciever_email']").on('blur' , function(event) {
                    var email = $(this).val();
                    var res = EmailCheck(email);
                    if (!res)
                    {

                        $("input[name*='wk_submit']").attr("disabled", true);
                        $(this).parent().addClass('has-error');
                        $(this).parent().find('p').show();
                    }
                    else{
                        $(this).parent().removeClass('has-error');
                        $(this).parent().find('p').hide();
                        $("input[name*='wk_submit']").attr("disabled", false);
                    }
                    
            });
            function EmailCheck(email) {
                if (!ValidateEmail(email)) {
                    return false;
                } else {
                    return true;
                }
            }
            function ValidateEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            }
        });

 });
});
