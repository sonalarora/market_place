# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################

from odoo import http
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
import logging
_logger = logging.getLogger(__name__)

class WebsiteSale(WebsiteSale):

    @http.route(['/shop/address'], type='http', methods=['GET', 'POST'], auth="public", website=True)
    def address(self, **kw):
        partnerId = int(kw.get('partner_id', -1))
        res = super(WebsiteSale, self).address(**kw)
        if partnerId > 0:
            partnerObj = request.env['res.partner'].sudo().browse(partnerId)
            if 'submitted' not in kw:
                res.qcontext.update({
                    'partner_latitude': partnerObj.partner_latitude,
                    'partner_longitude': partnerObj.partner_longitude,
                })
            else:
                self.updateLatLong(partnerObj, kw)
        return res

    def _checkout_form_save(self, mode, checkout, all_values):
        res = super(WebsiteSale, self)._checkout_form_save(mode, checkout, all_values)
        partnerId = int(all_values.get('partner_id', -1))
        if partnerId == -1:
            partnerObj = request.env['res.partner'].sudo().browse(res)
            if 'submitted' in all_values:
                self.updateLatLong(partnerObj, all_values)
        return res

    def updateLatLong(self, partnerObj, addressArray):
        partnerObj.write({
            'partner_latitude': addressArray.get('partner_latitude', False) or partnerObj.partner_latitude,
            'partner_longitude': addressArray.get('partner_longitude', False) or partnerObj.partner_longitude
        })
        return True
