# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################

import logging
_logger = logging.getLogger(__name__)

import odoo
from odoo import api, http, tools, _ # alphabetically ordered
from odoo.http import request

from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website_sale.controllers.main import WebsiteSale,TableCompute


PPG = 20  # Products Per Page
PPR = 4   # Products Per Row

class WebsiteSale(WebsiteSale):

    def _get_trending_search_domain(self, search, category, attrib_values):
        domain = self._get_search_domain(search, category, attrib_values)
        config_setting_obj = request.env['res.config.settings'].sudo().get_values()
        view_count_threshold = config_setting_obj.get('threshold_view_count') if config_setting_obj.get('threshold_view_count',False) else 0
        domain += [('view_count','>=',view_count_threshold)]
        return domain

    @http.route([
        '/trending/products',
        '/trending/products/page/<int:page>',
    ], type='http', auth="public", website=True)
    def trending_products(self, page=0, category=None, search='', ppg=False, **post):
        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        attrib_set = {v[1] for v in attrib_values}

        domain = self._get_trending_search_domain(search, category, attrib_values)

        keep = QueryURL('/trending/products', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))

        pricelist_context, pricelist = self._get_pricelist_context()

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)

        url = "/trending/products"
        if search:
            post["search"] = search
        if category:
            category = request.env['product.public.category'].browse(int(category))
            url = "/trending/products/category/%s" % slug(category)
        if attrib_list:
            post['attrib'] = attrib_list

        categs = request.env['product.public.category'].search([('parent_id', '=', False)])
        Product = request.env['product.template']

        parent_category_ids = []
        if category:
            parent_category_ids = [category.id]
            current_category = category
            while current_category.parent_id:
                parent_category_ids.append(current_category.parent_id.id)
                current_category = current_category.parent_id

        product_count = Product.search_count(domain)
        pager = request.website.pager(url=url, total=product_count, page=page, step=ppg, scope=7, url_args=post)
        products = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))

        ProductAttribute = request.env['product.attribute']
        if products:
            # get all products without limit
            selected_products = Product.search(domain, limit=False)
            attributes = ProductAttribute.search([('attribute_line_ids.product_tmpl_id', 'in', selected_products.ids)])
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        values = {
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'search_count': product_count,  # common for all searchbox
            'bins': TableCompute().process(products, ppg),
            'rows': PPR,
            'categories': categs,
            'attributes': attributes,
            'keep': keep,
            'parent_category_ids': parent_category_ids,
            'is_trending_page': True,
        }
        if category:
            values['main_object'] = category
        return request.render("odoo_trending_products.trending_product_page_template", values)

class ProductViewCount(http.Controller):

    @http.route(['/product/view/count'], type='json', auth="public", methods=['POST'], website=True)
    def product_view_count(self, product_id, **post):
        product = request.env["product.template"].browse(int(product_id))
        if product.website_published == True and product.sale_ok == True:
            product.sudo().view_count += 1
        return True

    @http.route('/shop/products/most_viewed', type='json', auth='public', website=True)
    def most_viewed_products(self, **kwargs):
        most_viewed_products = request.env["website"].sudo().get_most_viewed_products()
        if most_viewed_products:
            products_ids = most_viewed_products.mapped(lambda p: p._get_first_possible_variant_id())
            if products_ids:
                viewed_products = request.env['product.product'].browse(products_ids)
                FieldMonetary = request.env['ir.qweb.field.monetary']
                monetary_options = {
                    'display_currency': request.website.get_current_pricelist().currency_id,
                }
                rating = request.website.viewref('website_sale.product_comment').active
                res = {'products': []}
                for product in viewed_products:
                    combination_info = product._get_combination_info_variant()
                    res_product = product.read(['id', 'name', 'website_url'])[0]
                    res_product.update(combination_info)
                    res_product['price'] = FieldMonetary.value_to_html(res_product['price'], monetary_options)
                    if rating:
                        res_product['rating'] = request.env["ir.ui.view"].render_template('website_rating.rating_widget_stars_static', values={
                            'rating_avg': product.rating_avg,
                            'rating_count': product.rating_count,
                        })
                    res['products'].append(res_product)
                return res
        return {}
