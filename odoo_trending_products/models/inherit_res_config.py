# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    trending_product_limit = fields.Integer("Top Trending Products", default=0, help="Enter the limit of top trending products.")
    threshold_view_count = fields.Integer("Minimum Views Limit", default=0, help="Minimum views count value after which product will be in trending")

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.default'].sudo().set(
            'res.config.settings', 'trending_product_limit', self.trending_product_limit)
        self.env['ir.default'].sudo().set(
            'res.config.settings', 'threshold_view_count', self.threshold_view_count)

    @api.model
    def get_values(self):
        rec = super(ResConfigSettings, self).get_values()
        trending_product_limit = self.env['ir.default'].get(
            'res.config.settings', 'trending_product_limit')
        threshold_view_count = self.env['ir.default'].get(
            'res.config.settings', 'threshold_view_count')

        rec.update({
            'trending_product_limit' : trending_product_limit,
            'threshold_view_count' : threshold_view_count,
        })
        return rec

    def execute(self):
        for rec in self:
            if rec.trending_product_limit < 0:
                raise UserError(_("Top trending products value can't be negative."))
            if rec.threshold_view_count < 0:
                raise UserError(_("Minimum Views Limit can't be negative."))
        return super(ResConfigSettings, self).execute()
