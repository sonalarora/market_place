# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import api, fields, models, _

import logging
_logger = logging.getLogger(__name__)

class Website(models.Model):
    _inherit = 'website'

    @api.model
    def get_most_viewed_products(self):
        config_setting_obj = self.env['res.config.settings'].get_values()
        limit = config_setting_obj.get('trending_product_limit') if config_setting_obj.get('trending_product_limit',False) else 0
        view_count_threshold = config_setting_obj.get('threshold_view_count') if config_setting_obj.get('threshold_view_count',False) else 0
        most_viewed_list = self.env["product.template"].search([("sale_ok", "=", True),('website_published','=',True),('view_count','>=',view_count_threshold)],order="view_count desc", limit=limit) if limit else False
        return  most_viewed_list

    @api.model
    def get_most_viewed_product_ids(self):
        config_setting_obj = self.env['res.config.settings'].get_values()
        view_count_threshold = config_setting_obj.get('threshold_view_count') if config_setting_obj.get('threshold_view_count',False) else 0
        product_ids = self.env["product.template"].search([("sale_ok", "=", True),('website_published','=',True),('view_count','>=',view_count_threshold)],order="view_count desc").ids
        return product_ids
