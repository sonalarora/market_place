# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import api, fields, models, _ # alphabetically ordered
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)

class ProductTemplate(models.Model):
    _inherit = "product.template"

    view_count = fields.Integer("Product Views", help="Total number of views of this product on website.")

    @api.model
    def create(self, vals):
        if vals.get('view_count') and vals.get('view_count') < 0:
            raise UserError(_("Product Views can't be negative."))
        return super(ProductTemplate, self).create(vals)

    def write(self, vals):
        if vals.get('view_count') and vals.get('view_count') < 0:
            raise UserError(_("Product Views can't be negative."))
        return super(ProductTemplate, self).write(vals)
