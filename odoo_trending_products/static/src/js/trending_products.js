/* Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>) */
/* See LICENSE file for full copyright and licensing details. */
/* License URL : https://store.webkul.com/license.html/ */

odoo.define('odoo_trending_products.trending_products', function (require) {

    var publicWidget = require('web.public.widget');
    var concurrency = require('web.concurrency');
    var config = require('web.config');
    var core = require('web.core');
    var utils = require('web.utils');
    var wSaleUtils = require('website_sale.utils');

    var qweb = core.qweb;    

    publicWidget.registry.wkMostViewedProductsSmSnippet = publicWidget.Widget.extend({
        selector: '.mv_small_snippet_carousel',
        xmlDependencies: ['/odoo_trending_products/static/src/xml/trending_products_snippet.xml'],
        disabledInEditableMode: false,
        events: {
            'click .js_add_cart': '_onAddToCart',
        },

        init: function () {
            this._super.apply(this, arguments);
            this._dp = new concurrency.DropPrevious();
            this.uniqueId = _.uniqueId('o_carousel_mv_products_small_');
            this._onResizeChange = _.debounce(this._addCarousel, 100);
        },

        start: function () {
            this._dp.add(this._fetch()).then(this._render.bind(this));
            $(window).resize(() => {
                this._onResizeChange();
            });
            return this._super.apply(this, arguments);
        },

        destroy: function () {
            this._super(...arguments);
            this.$el.addClass('d-none');
            this.$el.find('.slider').html('');
        },

        _fetch: function () {
            return this._rpc({
                route: '/shop/products/most_viewed',
            }).then(res => {
                var products = res['products'];
                return res;
            });
        },

        _render: function (res) {
            var products = res['products'];
            var mobileProducts = [];
            _.each(products, function (product) {
                mobileProducts.push([product]);
            });
            this.smallCarousel = $(qweb.render('odoo_trending_products.mostViewedProducts', {
                uniqueId: this.uniqueId,
                productFrame: 1,
                productsGroups: mobileProducts,
                mv_type: 'small',
            }));
            this._addCarousel();
            this.$el.toggleClass('d-none', !(products && products.length));
        },

        _addCarousel: function () {
            var carousel = this.smallCarousel;
            this.$('.slider').html(carousel).css('display', '');
        },

        _onAddToCart: function (ev) {
            var self = this;
            var $card = $(ev.currentTarget).closest('.card');
            this._rpc({
                route: "/shop/cart/update_json",
                params: {
                    product_id: $card.find('input[data-product-id]').data('product-id'),
                    add_qty: 1
                },
            }).then(function (data) {
                wSaleUtils.updateCartNavBar(data);
                var $navButton = wSaleUtils.getNavBarButton('.o_wsale_my_cart');
                var fetch = self._fetch();
                var animation = wSaleUtils.animateClone($navButton, $(ev.currentTarget).parents('.o_carousel_product_card'), 25, 40);
                Promise.all([fetch, animation]).then(function (values) {
                    self._render(values[0]);
                });
            });
        },
    });

    publicWidget.registry.wkMostViewedProductsLgSnippet = publicWidget.Widget.extend({
        selector: '.mv_large_snippet_carousel',
        xmlDependencies: ['/odoo_trending_products/static/src/xml/trending_products_snippet.xml'],
        disabledInEditableMode: false,
        events: {
            'click .js_add_cart': '_onAddToCart',
        },

        init: function () {
            this._super.apply(this, arguments);
            this._dp = new concurrency.DropPrevious();
            this.uniqueId = _.uniqueId('o_carousel_mv_products_large_');
            this._onResizeChange = _.debounce(this._addCarousel, 100);
        },

        start: function () {
            this._dp.add(this._fetch()).then(this._render.bind(this));
            $(window).resize(() => {
                this._onResizeChange();
            });
            return this._super.apply(this, arguments);
        },

        destroy: function () {
            this._super(...arguments);
            this.$el.addClass('d-none');
            this.$el.find('.slider').html('');
        },

        _fetch: function () {
            return this._rpc({
                route: '/shop/products/most_viewed',
            }).then(res => {
                return res;
            });
        },

        _render: function (res) {
            var products = res['products'];
            var mobileProducts = [], webProducts = [], productsTemp = [];
            _.each(products, function (product) {
                if (productsTemp.length === 4) {
                    webProducts.push(productsTemp);
                    productsTemp = [];
                }
                productsTemp.push(product);
                mobileProducts.push([product]);
            });
            if (productsTemp.length) {
                webProducts.push(productsTemp);
            }
            this.mobileCarousel = $(qweb.render('odoo_trending_products.mostViewedProducts', {
                uniqueId: this.uniqueId,
                productFrame: 1,
                productsGroups: mobileProducts,
                mv_type: 'large',
            }));
            this.webCarousel = $(qweb.render('odoo_trending_products.mostViewedProducts', {
                uniqueId: this.uniqueId,
                productFrame: 4,
                productsGroups: webProducts,
                mv_type: 'large',
            }));
            this._addCarousel();
            this.$el.toggleClass('d-none', !(products && products.length));
        },
        _addCarousel: function () {
            var carousel = config.device.size_class <= config.device.SIZES.SM ? this.mobileCarousel : this.webCarousel;
            this.$('.slider').html(carousel).css('display', ''); // TODO removing the style is useless in master
        },

        _onAddToCart: function (ev) {
            var self = this;
            var $card = $(ev.currentTarget).closest('.card');
            this._rpc({
                route: "/shop/cart/update_json",
                params: {
                    product_id: $card.find('input[data-product-id]').data('product-id'),
                    add_qty: 1
                },
            }).then(function (data) {
                wSaleUtils.updateCartNavBar(data);
                var $navButton = wSaleUtils.getNavBarButton('.o_wsale_my_cart');
                var fetch = self._fetch();
                var animation = wSaleUtils.animateClone($navButton, $(ev.currentTarget).parents('.o_carousel_product_card'), 25, 40);
                Promise.all([fetch, animation]).then(function (values) {
                    self._render(values[0]);
                });
            });
        },
    });

    publicWidget.registry.wkMostViewedProductsUpdate = publicWidget.Widget.extend({
        selector : '#product_view_count',
        debounceValue: 3000,

        init: function () {
            this._super.apply(this, arguments);
            this._updateProductView = _.debounce(this._updateProductView, this.debounceValue);
        },

        start: function () {
            this._updateProductView();
            return this._super.apply(this, arguments);
        },

        _updateProductView: function () {
            var product_id = parseInt($('#product_view_count').val());
            this._rpc({
                route: '/product/view/count',
                params: {
                    product_id: product_id,
                }
            });
        },
    });

});
