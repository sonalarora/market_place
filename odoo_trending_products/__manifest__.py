# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Website Most Viewed Products",
  "summary"              :  """The module allows you to display the most viewed products on the Odoo website.""",
  "category"             :  "Website",
  "version"              :  "1.0.0",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "website"              :  "https://store.webkul.com/Odoo-Website-Most-Viewed-Products.html",
  "description"          :  """Odoo Website Most Viewed Products
Related products on product page
Website related products
Related products odoo
Odoo related products
Other accessories
You may also like
Customers also bought
Seller products
Odoo other products
Similar products odoo
Odoo Same products
Product page similar products
Related accessory product
Suggested Accessories""",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=odoo_trending_products&custom_url=/shop",
  "depends"              :  ['website_sale'],
  "data"                 :  [
                             'views/inherit_product_template_view.xml',
                             'views/trending_products_templates.xml',
                             'views/inherit_res_config_view.xml',
                             'views/most_view_product_template.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  45,
  "currency"             :  "USD",
  "pre_init_hook"        :  "pre_init_check",
}