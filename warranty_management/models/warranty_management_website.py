# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

# from odoo import api, fields, models, _
# from odoo.exceptions import Warning

# class Webiste(models.Model):
#     _inherit = 'website'

#     days_before = fields.Integer("Before")
#     renewal_prod = fields.Many2one(
#         'product.product', string="Warranrt Renewal Product", required=True)
#     warranty_expire_notification = fields.Boolean(
#         string='Warranty Expire Notification',
#         help='A notifcation mail will be sent to the customer before entered from warranty expire.')
