# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import api, fields, models, _
from odoo.http import request
from odoo.exceptions import ValidationError

class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_warranty = fields.Boolean(string='Allow Warranty')
    warranty_type = fields.Selection([
        ('free', 'Free'),
        ('paid', 'Paid')], default="free", string='Warranty Type')
    warranty_auto_confirm = fields.Boolean(
        string="Auto Warranty Confirm",
        help="If set 'True' then warranty will get auto confirm")
    warranty_fee = fields.Float(string="Warranty Fee" )
    warranty_period = fields.Integer(string="Period", default=6)
    warranty_unit = fields.Selection([
        ('days', 'Days'),
        ('months', 'Month'),
        ('years', 'Year')], default="months", string="Unit")
    allow_renewal = fields.Boolean(string="Can Be Renew")
    renewal_period = fields.Integer(string="Renewal Period", default=6)
    renewal_unit = fields.Selection([
        ('days', 'Days'),
        ('months', 'Month'),
        ('years', 'Year')], default="months", string="Unit")
    max_renewal_times = fields.Integer(string="Max. Renewal Times", default=1)
    renewal_cost = fields.Float(string="Renewal Cost")
    website_warranty_fee = fields.Float(string="Website Warranty Fee")
    

    @api.constrains('warranty_period')
    def _check_warranty_period(self):
        minimum_days = request.env['ir.config_parameter'].sudo().get_param('warranty_management.days_before')
        warranty_expire_notification = request.env['ir.config_parameter'].sudo().get_param('warranty_management.warranty_expire_notification')
        if warranty_expire_notification:
            if int(minimum_days) >= self.warranty_period and self.warranty_unit == 'days':
                raise ValidationError(_('The warranty days should be more then days of expire notification (%s days).'%minimum_days))


    @api.model
    def create(self, vals):
        _ = dict(self._context or {})
        return super().create(vals)

    def write(self, vals):
        _ = dict(self._context or {})
        return super().write(vals)
