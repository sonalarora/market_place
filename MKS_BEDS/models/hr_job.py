# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class BIDSJob(models.Model):
    _name = "hr.bids.job"
    _inherit = ['hr.job','mail.alias.mixin']
    # _order = "state desc, name asc"

    @api.model
    def _default_address_id(self):
        return self.env.company.partner_id

    def _get_default_favorite_bids_user_ids(self):
        return [(6, 0, [self.env.uid])]

    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
    state = fields.Selection([
        ('recruit', 'Recruitment in Progress'),
        ('open', 'Not Recruiting')
    ], string='Status', readonly=True, required=True, tracking=True, copy=False, default='recruit', help="Set whether the recruitment process is open or closed for this job position.")
    address_id = fields.Many2one(
        'res.partner', "Job Location", default=_default_address_id,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="Address where employees are working")
    application_ids = fields.One2many('hr.bids.applicant', 'job_id', "Applications")
    application_count = fields.Integer(compute='_compute_application_count', string="Application Count")
    new_application_count = fields.Integer(
        compute='_compute_new_application_count', string="New Application",
        help="Number of applications that are new in the flow (typically at first step of the flow)")
    manager_id = fields.Many2one(
        'hr.employee', related='department_id.manager_id', string="Department Manager", readonly=True, store=True)
    user_id = fields.Many2one('res.users', "Responsible", tracking=True)
    hr_responsible_id = fields.Many2one(
        'res.users', "HR Responsible", tracking=True,
        help="Person responsible of validating the employee's contracts.")
    document_ids = fields.One2many('ir.attachment', compute='_compute_document_ids', string="Documents")
    documents_count = fields.Integer(compute='_compute_document_ids', string="Document Count")
    alias_id = fields.Many2one(
        'mail.alias', "Alias", ondelete="restrict", required=True,
        help="Email alias for this job position. New emails will automatically create new applicants for this job position.")
    color = fields.Integer("Color Index")
    is_favorite = fields.Boolean(compute='_compute_is_favorite', inverse='_inverse_is_favorite')
    favorite_user_ids = fields.Many2many('res.users', 'bids_job_favorite_user_rel', 'bids_job_id', 'bids_user_id', default=_get_default_favorite_bids_user_ids)
    no_of_recruitment = fields.Integer(string='Expected New Employees', copy=False,
        help='Number of new employees you expect to recruit.', default=1)

    def _compute_is_favorite(self):
        for job in self:
            job.is_favorite = self.env.user in job.favorite_user_ids

    def _inverse_is_favorite(self):
        unfavorited_jobs = favorited_jobs = self.env['hr.bids.job']
        for job in self:
            if self.env.user in job.favorite_user_ids:
                unfavorited_jobs |= job
            else:
                favorited_jobs |= job
        favorited_jobs.write({'favorite_user_ids': [(4, self.env.uid)]})
        unfavorited_jobs.write({'favorite_user_ids': [(3, self.env.uid)]})

    def _compute_document_ids(self):
        print('test++++++++++++++++')
        applicants = self.mapped('application_ids').filtered(lambda self: not self.employee_id.id)
        print('applicants%%%%%%%%%%%%%%%%%%%')
        app_to_job = dict((applicant.id, applicant.job_id.id) for applicant in applicants)
        attachments = self.env['ir.attachment'].search([
            '|',
            '&', ('res_model', '=', 'hr.bids.job'), ('res_id', 'in', self.ids),
            '&', ('res_model', '=', 'hr.bids.applicant'), ('res_id', 'in', applicants.ids)])
        result = dict.fromkeys(self.ids, self.env['ir.attachment'])
        for attachment in attachments:
            if attachment.res_model == 'hr.bids.applicant':
                result[app_to_job[attachment.res_id]] |= attachment
            else:
                result[attachment.res_id] |= attachment

        for job in self:
            job.document_ids = result[job.id]
            job.documents_count = len(job.document_ids)

    def _compute_application_count(self):
        read_group_result = self.env['hr.bids.applicant'].read_group([('job_id', 'in', self.ids)], ['job_id'], ['job_id'])
        result = dict((data['job_id'][0], data['job_id_count']) for data in read_group_result)
        for job in self:
            job.application_count = result.get(job.id, 0)

    def _get_first_stage(self):
        self.ensure_one()
        return self.env['hr.bids.stage'].search([
            '|',
            ('job_ids', '=', False),
            ('job_ids', '=', self.id)], order='sequence asc', limit=1)

    def _compute_new_application_count(self):
        for job in self:
            job.new_application_count = self.env["hr.bids.applicant"].search_count(
                [("job_id", "=", job.id), ("stage_id", "=", job._get_first_stage().id)]
            )

    def get_alias_model_name(self, vals):
        return 'hr.bids.applicant'

    def get_alias_values(self):
        values = super(BIDSJob, self).get_alias_values()
        values['alias_defaults'] = {
            'job_id': self.id,
            'department_id': self.department_id.id,
            'company_id': self.department_id.company_id.id if self.department_id else self.company_id.id
        }
        return values

    @api.model
    def create(self, vals):
        vals['favorite_user_ids'] = vals.get('favorite_user_ids', []) + [(4, self.env.uid)]
        return super(BIDSJob, self).create(vals)

    # def _creation_subtype(self):
    #     print('test++++++++++++++++++++++++')
    #     return self.env.ref('MKS_BEDS.mt_job_new_bids')

    def action_get_attachment_tree_view(self):
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('MKS_BEDS.ir_attachment_view_search_inherit_hr_recruitment').id, )
        action['domain'] = ['|', '&', ('res_model', '=', 'hr.bids.job'), ('res_id', 'in', self.ids), '&', ('res_model', '=', 'hr.bids.applicant'), ('res_id', 'in', self.mapped('application_ids').ids)]
        return action

    def close_dialog(self):
        return {'type': 'ir.actions.act_window_close'}

    def edit_dialog(self):
        form_view = self.env.ref('hr.view_hr_job_form')
        return {
            'name': _('Job'),
            'res_model': 'hr.bids.job',
            'res_id': self.id,
            'views': [(form_view.id, 'form'),],
            'type': 'ir.actions.act_window',
            'target': 'inline'
        }
