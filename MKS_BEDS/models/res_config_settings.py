# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = ['res.config.settings']

    module_website_hr_bids = fields.Boolean(string='Online Posting')
    module_hr_bids_survey = fields.Boolean(string='Interview Forms')
