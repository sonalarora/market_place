
====================================================
Price Filter - ecommerce product price filter
====================================================

Filter by price in shop.

=========
Features
=========

- Filter option on Shop page to filter products by price.
- No need to configure to set Min and Max price.
- Price filter enable or disable option for admin.
- Quick load products based on slider min max range.

============
Similar Apps
============

Price slider
price range slider odoo
odoo range slider
odoo slider for price
Odoo shop filter
shop price filter
filterby price odoo
filterby price
product price
price filter
ecommerce filter
