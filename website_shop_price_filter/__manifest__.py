# Part of odoo. See LICENSE file for full copyright and licensing details

{
    # Module Information
    'name' : 'Price Filter',
    'category' : 'Website',
    'version' : '13.0.1.0.0',
    'license': 'OPL-1',
    'summary': 'Product Filterby Price on Shop',
    'description': """
        Product Filterby Price on Shop using price range slider.
    """,
    # Dependencies
    'depends': [
        'website_sale',
    ],
    # Views
    'data': [
        'views/assets.xml',
        'views/shop_templates.xml'
    ],
    # Author
    'author': 'Techinsider Solution',
    'website': 'http://www.techinsidersolution.com',
    # App Store Specific
    'images': ['static/description/price_range_poster.png'],
    # Pricing
    'price': 19,
    'currency': 'EUR',
    # Technical
    'installable': True,
}
