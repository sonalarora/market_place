# Part of odoo. See LICENSE file for full copyright and licensing details
from odoo import http
from odoo.http import request
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale.controllers.main import TableCompute
from odoo.addons.http_routing.models.ir_http import slug
from odoo.osv import expression


class WebsiteSaleExt(WebsiteSale):

    def _get_search_domain_extended(self, search, category, attrib_values,
                               price_min=None, price_max=None):
        domains = [request.website.sale_product_domain()]
        if search:
            for srch in search.split(" "):
                subdomains = [
                    [('name', 'ilike', srch)],
                    [('product_variant_ids.default_code', 'ilike', srch)]
                ]
                if search_in_description:
                    subdomains.append([('description', 'ilike', srch)])
                    subdomains.append([('description_sale', 'ilike', srch)])
                domains.append(expression.OR(subdomains))
        if category:
            domains.append([('public_categ_ids', 'child_of', int(category))])
        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domains.append([('attribute_line_ids.value_ids', 'in', ids)])
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domains.append([('attribute_line_ids.value_ids', 'in', ids)])
        if price_min or price_max:
            domains.append([('list_price', '>=', price_min),
                       ('list_price', '<=', price_max)])
        return expression.AND(domains)

    @http.route()
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        add_qty = int(post.get('add_qty', 1))
        Category = request.env['product.public.category']
        if category:
            category = Category.search([('id', '=', int(category))], limit=1)
            if not category or not category.can_access_from_current_website():
                raise NotFound()
        else:
            category = Category
        if ppg:
            try:
                ppg = int(ppg)
                post['ppg'] = ppg
            except ValueError:
                ppg = False
        if not ppg:
            ppg = request.env['website'].get_current_website().shop_ppg or 20
        ppr = request.env['website'].get_current_website().shop_ppr or 4

        # For Attributes
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]
        attributes_ids = {v[0] for v in attrib_values}
        attrib_set = {v[1] for v in attrib_values}
        domain = self._get_search_domain_extended(search, category, attrib_values)
        Product = request.env['product.template'].with_context(bin_size=True)

        # For Price Filter
        categ_products = None
        if category:
            domain.append(('public_categ_ids', 'child_of', category.id))
            categ_products = Product.search(domain)
            if categ_products:
                price_max, price_min, price_max_range, price_min_range \
                    = self.get_min_max_pricelist(
                    categ_products)
            else:
                price_max = price_min = price_min_range = price_max_range = 0.0
        else:
            domain = self._get_search_domain_extended(search, category,
                                                 attrib_values)
            categ_products = Product.search(domain)
            if categ_products:
                price_max, price_min, price_max_range, price_min_range = self.get_min_max_pricelist(
                    categ_products)
            else:
                price_max = price_min = price_min_range = price_max_range = 0.0

        price_max_domain = request.website.get_current_pricelist().currency_id.compute(
            price_max, request.website.company_id.currency_id)
        price_min_domain = request.website.get_current_pricelist().currency_id.compute(
            price_min, request.website.company_id.currency_id)
        domain = self._get_search_domain_extended(search, category, attrib_values,
                                             price_min_domain, price_max_domain)
        keep = QueryURL('/shop', category=category and int(category),
                        search=search, attrib=attrib_list,
                        order=post.get('order'),
                        price_min=price_min,
                        price_max=price_max)
        url = "/shop"
        if category:
            category = request.env['product.public.category'].browse(
                int(category))
            url = "/shop/category/%s" % slug(category)
        product_count = Product.search_count(domain)
        pager = request.website.pager(url=url, total=product_count,
                                      page=page, step=ppg, scope=7,
                                      url_args=post)
        products = Product.search(domain, limit=ppg, offset=pager['offset'],
                                  order=self._get_search_order(post))
        ProductAttribute = request.env['product.attribute']
        if products:
            attributes = ProductAttribute.search(
                [('product_tmpl_ids', 'in', products.ids)])
        else:
            attributes = ProductAttribute.browse(attributes_ids)
            
        layout_mode = request.session.get('website_sale_shop_layout_mode')
        if not layout_mode:
            if request.website.viewref('website_sale.products_list_view').active:
                layout_mode = 'list'
            else:
                layout_mode = 'grid'
        res = super(WebsiteSaleExt, self).shop(page=page, category=category,
                                               search=search, ppg=ppg, **post)
        res.qcontext.update({
            'pager': pager, 'products': products,
            'bins': TableCompute().process(products, ppg),
            'attributes': attributes, 'search_count': product_count,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'PPG': ppg, 'price_min_range': price_min_range,
            'price_max_range': price_max_range, 'price_min': price_min,
            'price_max': price_max, 'keep': keep,
            'categ_products': categ_products,
        })
        return res

    def get_min_max_pricelist(self, products):
        products_price = [product.list_price
                          for product in products]
        products_price.sort()
        price_min_range = products_price and products_price[0]
        price_max_range = products_price and products_price[-1]
        if request.httprequest.args.getlist('price_min') \
                and request.httprequest.args.getlist(
            'price_min')[0] != '':
            price_min = float(request.httprequest.args.getlist(
                'price_min')[0])
        else:
            price_min = float(price_min_range)
        if request.httprequest.args.getlist('price_max') \
                and request.httprequest.args.getlist(
            'price_max')[0] != '':
            price_max = float(request.httprequest.args.getlist(
                'price_max')[0])
        else:
            price_max = float(price_max_range)
        return price_max, price_min, price_max_range, price_min_range

    @http.route()
    def product(self, product, category='', search='', **kwargs):
        ProductCategory = request.env['product.public.category']
        if category:
            category = ProductCategory.browse(int(category)).exists()

        # For Attributes
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        if request.httprequest.args.getlist('price_min') \
                and request.httprequest.args.getlist('price_min')[0] != '':
            price_min = float(request.httprequest.args.getlist('price_min')[0])
        else:
            price_min = False

        if request.httprequest.args.getlist('price_max') \
                and request.httprequest.args.getlist('price_max')[0] != '':
            price_max = float(request.httprequest.args.getlist('price_max')[0])
        else:
            price_max = False

        keep = QueryURL('/shop', category=category and category.id,
                        search=search, attrib=attrib_list,
                        price_min=price_min,
                        price_max=price_max)
        res = super(WebsiteSaleExt, self).product(product=product,
                                                  category=category,
                                                  search=search, **kwargs)
        res.qcontext.update({
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'keep': keep,
            'price_min': price_min,
            'price_max': price_max,
        })
        return res
